/**
 * @author Timur Berezhnoi <TimurJD>
 */
(function() {
    'use strict';

    angular.module('angularStentle', [])
        .factory('AngularStentle', AngularStentle);

    AngularStentle.$inject = ['$http', 'ENV'];

    function AngularStentle($http, constant) {

        var AngularStentle = {
            login                  : login,
            logout                 : logout,
            getBlocksList          : getBlocksList,
            getPoliciesList        : getPoliciesList,
            getOrders              : getOrders,
            getProducts            : getProducts,
            getAccounts            : getAccounts,
            getDeals               : getDeals,
            getProductDetails      : getProductDetails,
            getOrderDetails        : getOrderDetails,
            getCustomersList       : getCustomersList,
            getCustomerDetails     : getCustomerDetails,
            updateCustomer         : updateCustomer,
            declineDeal            : declineDeal,
            createProduct          : createProduct,
            publishProduct         : publishProduct,
            getConfigs             : getConfigs,
            getMicroCategories     : getMicroCategories,
            updateProduct          : updateProduct,
            getOrdersByCustomerId  : getOrdersByCustomerId,
            uploadProductCoverPhoto: uploadProductCoverPhoto,
            deleteProduct          : deleteProduct,
            raiseCounterOffer      : raiseCounterOffer,
            acceptDeal             : acceptDeal,
            fetchBrand             : fetchBrand
        };

        return AngularStentle;

        function login(userInfo, callback) {
            var path = '/rest-dev/picnik-rest/login?username=' + userInfo.email + '&password=' + userInfo.password + '&remember-me=' + true;
            doPost(path, userInfo, callback);
        }

        function logout(callback) {
            var path = '/rest-dev/picnik-rest/logout';
            doPost(path, null, callback);
        }

        function getPoliciesList(callback) {
            var path = '/rest-dev/picnik-rest/backoffice/policies';
            doGet(path, callback);
        }

        function getBlocksList(callback) {
            var path = '/rest-dev/picnik-rest/backoffice/dashboard';
            doGet(path, callback);
        }

        function getOrders(limit, pageNumber, callback) {
            var path = '/rest-dev/picnik-rest/backoffice/orders?limit=' + limit + '&pageNumber=' + pageNumber;
            doGet(path, callback);
        }

        function getProducts(limit, pageNumber, callback) {
            var path = '/rest-dev/picnik-rest/backoffice/products?limit=' + limit + '&pageNumber=' + pageNumber;
            doGet(path, callback);
        }

        function getAccounts(callback) {
            var path = '/rest-dev/picnik-rest/backoffice/users/me';
            doGet(path, callback);
        }

        function getDeals(role, callback) {
            var path;
            if(role === 'ROLE_RETAILER') {
                path = ':8090/merchants/56c5fffb8305517a4f05939a/deals';
            } else {
                path = ':8090/merchants/56c5fffb8305517a4f05939a/deals';
            }

            doGet(path, callback);
        }

        function getProductDetails(productId, callback) {
            var path = '/rest-dev/picnik-rest/products/' + productId;
            doGet(path, callback);
        }

        function getOrderDetails(orderId, callback) {
            var path = '/rest-dev/picnik-rest/purchase-orders/' + orderId;
            doGet(path, callback);
        }

        function getCustomersList(limit, pageNumber, callback) {
            var path = '/rest-dev/picnik-rest/backoffice/customers?limit=' + limit + '&pageNumber=' + pageNumber;
            doGet(path, callback);
        }

        function getCustomerDetails(customerId, callback) {
            var path = '/rest-dev/picnik-rest/customers/' + customerId;
            doGet(path, callback);
        }

        function updateCustomer(id, customer, callback) {
            var path = '/rest-dev/picnik-rest/customers/' + id;
            doPut(path, customer, callback);
        }


        function declineDeal(dealId, callback) {
            var path = ':8090/merchants/56c5fffb8305517a4f05939a/deals/' + dealId + '/options';
            var data = {
                "hidden": true
            };
            doPost(path, data, callback);
        }

        function createProduct(product, callback) {
            var path = '/rest-dev/picnik-rest/products';
            doPost(path, product, callback);
        }

        function publishProduct(productId, callback) {
            var path = '/rest-dev/picnik-rest/products/' + productId + '/publish/true';
            var data = {
                productId: productId
            };

            doPut(path, data, callback);
        }

        function acceptDeal(dealId, callback) {
            var path = ':8090/merchants/56c5fffb8305517a4f05939a/deals/' + dealId + '/counter-offers';
            var data = {
                "_COMMENT_"     : "VALUE OF MERCHANT MUST BE THE SAME MERCHANT CORRELATED TO BACK OFFICE USER, THIS LINE OF CODE MUST BE REMOVED",
                "merchant"      : {
                    "id"          : "56c5fffb8305517a4f05939c",
                    "name"        : "Merchant 3",
                    "primaryEmail": "merchant.3@mail.com"
                },
                "accepted"      : true,
                "expirationDate": "2016-05-01 00:00 GMT"
            };
            doPost(path, data, callback);
        }

        // Retrieve configs where can be found macrocategories list
        function getConfigs(callback) {
            var path = '/rest-dev/picnik-rest/configs';
            doGet(path, callback)
        }

        function getMicroCategories(macroCategoryKey, callback) {
            var path = '/rest-dev/picnik-rest/products/micro-category-list?parentKey=' + macroCategoryKey;
            doGet(path, callback)
        }

        function updateProduct(productId, data, callback) {
            var path = '/rest-dev/picnik-rest/products/' + productId;
            doPut(path, data, callback);
        }

        function getOrdersByCustomerId(customerId, limit, pageNumber, callback) {
            var path = '/rest-dev/picnik-rest/customers/' + customerId + '/purchase-orders?limit=' + limit + '&pageNumber=' + pageNumber;
            doGet(path, callback)
        }

        function uploadProductCoverPhoto(productId, photo, callback) {
            var path = '/rest-dev/picnik-rest/products/' + productId + '/product-photo/cover';
            var formData = new FormData();

            formData.append("fileUpload1", photo);

            $http({
                withCredentials : true,
                crossDomain     : true,
                url             : constant.DEV_URL + path,
                method          : 'POST',
                data            : formData,
                transformRequest: angular.identity,
                headers         : {'Content-Type': undefined}
            }).then(
                function(response) {
                    if(callback) {
                        callback(null, response.data);
                    }
                },
                function(response) {
                    if(callback) {
                        callback(response.data);
                    }
                });
        }

        function deleteProduct(productId, callback) {
            var path = '/rest-dev/picnik-rest/products/' + productId;
            doDelete(path, callback);
        }

        function raiseCounterOffer(dealId, newValue, callback) {
            var path = ':8090/merchants/56c5fffb8305517a4f05939a/deals/' + dealId + '/counter-offers';
            var data = {
                "expirationDate": "2016-01-03 00:00 GMT",
                "price"         : {
                    "value"   : newValue,
                    "currency": "EUR"
                },
                "accepted"      : false
            };
            doPost(path, data, callback);
        }

        function fetchBrand(macroCategory, microCategory, brand, callback) {
            var path = '/rest-dev/picnik-rest/brands?macroCategoryKey=' + macroCategory + '&microCategoryKey=' + microCategory + '&name=' + brand;
            $http({
                withCredentials: true,
                crossDomain    : true,
                url            : constant.DEV_URL + path,
                method         : 'GET',
                data           : ''
            }).then(
                function(response) {
                    if(callback) {
                        callback(null, response.data);
                    }
                },
                function(response) {
                    if(callback) {
                        callback(response.data);
                    }
                });
        }

        // Use doGet(path, callback) in every additional functions to avoid code-double
        function doGet(path, callback) {
            $http({
                withCredentials: true,
                crossDomain    : true,
                url            : constant.DEV_URL + path,
                method         : 'GET'
            }).then(
                function(response) {
                    if(callback) {
                        callback(null, response.data);
                    }
                },
                function(response) {
                    if(callback) {
                        callback(response.data);
                    }
                });
        }

        // Use doPost(path, data, callback) in every additional functions to avoid code-double
        function doPost(path, data, callback) {
            $http({
                withCredentials: true,
                crossDomain    : true,
                url            : constant.DEV_URL + path,
                data           : data,
                method         : 'POST'
            }).then(
                function(response) {
                    if(callback) {
                        callback(null, response.data);
                    }
                },
                function(response) {
                    if(callback) {
                        callback(response.data);
                    }
                });
        }

        // Use doPut(path, data, callback) in every additional functions to avoid code-double
        function doPut(path, data, callback) {
            $http({
                withCredentials: true,
                crossDomain    : true,
                url            : constant.DEV_URL + path,
                data           : data,
                method         : 'PUT'
            }).then(
                function(response) {
                    if(callback) {
                        callback(null, response.data);
                    }
                },
                function(response) {
                    if(callback) {
                        callback(response.data);
                    }
                });
        }

        function doDelete(path, callback) {
            $http({
                withCredentials: true,
                crossDomain    : true,
                url            : constant.DEV_URL + path,
                method         : 'DELETE'
            }).then(
                function(response) {
                    if(callback) {
                        callback(null, response.data);
                    }
                },
                function(response) {
                    if(callback) {
                        callback(response.data);
                    }
                });
        }
    }
})();